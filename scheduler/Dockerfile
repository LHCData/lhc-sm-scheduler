FROM cern/cc7-base:20191107

# Install HEP_OSlibs - which is required to use software from CVMFS
RUN yum -y install \
    HEP_OSlibs-7.2.7-1.el7.cern \
    gcc \
    git \
    python36-devel \
    yum clean all && rm -r /var/cache/yum/*

RUN cd /tmp && curl https://bootstrap.pypa.io/get-pip.py | python3.6

ARG DATEUTIL_VERSION=2.6.1
ARG AIRFLOW_VERSION=1.10.9
ARG SQLALCH_VERSION=1.3.15
ARG AIRFLOW_USER_HOME=/usr/local/airflow
ARG AIRFLOW_ETC=/etc/airflow

ENV AIRFLOW_HOME=${AIRFLOW_USER_HOME}
ENV LC_CTYPE en_US.UTF-8
ENV LANG en_US.UTF-8

ENV USER=airflow
ENV USER_ID=1001

RUN pip3.6 install \
    python-dateutil==$DATEUTIL_VERSION \
    apache-airflow[postgres]==$AIRFLOW_VERSION \
    SQLAlchemy==$SQLALCH_VERSION \
    airflow-code-editor \
    email_validator

COPY entrypoint.sh /entrypoint.sh
COPY plugins /usr/local/airflow-plugins

RUN useradd -ms /bin/bash -d ${AIRFLOW_HOME} --uid ${USER_ID} airflow

RUN mkdir -p ${AIRFLOW_ETC}
RUN chown -R airflow: ${AIRFLOW_ETC}
RUN chown -R airflow: ${AIRFLOW_HOME}

RUN chgrp root /etc/passwd
RUN chmod ug+rw /etc/passwd

USER ${USER_ID}
WORKDIR ${AIRFLOW_HOME}
ENTRYPOINT ["/entrypoint.sh"]

EXPOSE 8080