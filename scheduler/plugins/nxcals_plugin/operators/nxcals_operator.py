from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator
from airflow.exceptions import AirflowException
from airflow.models.variable import Variable

import subprocess
import os

class SparkNXCALSOperator(SparkSubmitOperator):

    def _prepare_spark_submit(self):
        if not os.path.exists('/cvmfs/sft.cern.ch'):
            raise AirflowException("NXCALS requires access to CVMFS")

        nxcals_api_keytab = Variable.get('nxcals-api-keytab-path', default_var=None)
        nxcals_api_principal = Variable.get('nxcals-api-principal', default_var=None)
        hadoop_keytab = Variable.get('hadoop-keytab-path', default_var=None)
        hadoop_principal = Variable.get('hadoop-principal', default_var=None)

        if not nxcals_api_keytab or not nxcals_api_principal or not hadoop_keytab or not hadoop_principal:
            raise AirflowException("NXCALS requires AirFlow Variables: nxcals-api-keytab-path, nxcals-api-principal, hadoop-keytab-path, hadoop-principal")

        # set spark-submit env
        submit_env_cmd = ';'.join([
            'source /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/setup.sh > /dev/null 2>&1',
            'export LCG_VIEW=/cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt > /dev/null 2>&1',
            'source /cvmfs/sft.cern.ch/lcg/etc/hadoop-confext/hadoop-swan-setconf.sh nxcals > /dev/null 2>&1',
            'env'
        ])
        p = subprocess.Popen(submit_env_cmd, shell=True, stdout=subprocess.PIPE, close_fds=True,
                             universal_newlines=True)
        for line in p.stdout:
            env_line = line.rstrip().split('=', 1)
            os.environ[env_line[0]] = env_line[1]

        self.log.info('Defined submit env: %s' % str(os.environ))

        self._conn_id = 'yarn-cluster'
        self._name = self.task_id

        self._conf = self._conf or {}
        self._conf['spark.sql.caseSensitive'] = 'true'
        self._conf['spark.sql.execution.arrow.enabled'] = 'true'
        self._conf['spark.yarn.submit.waitAppCompletion'] = 'true'

        # adjust PYTHONPATH on driver
        if 'spark.yarn.appMasterEnv.PYTHONPATH' in self._conf:
            self._conf['spark.yarn.appMasterEnv.PYTHONPATH'] = self._conf['spark.yarn.appMasterEnv.PYTHONPATH'] + ':' + os.environ['PYTHONPATH']
        else:
            self._conf['spark.yarn.appMasterEnv.PYTHONPATH'] = os.environ['PYTHONPATH']

        # adjust LD_LIBRARY_PATH on driver
        if 'spark.yarn.appMasterEnv.LD_LIBRARY_PATH' in self._conf:
            self._conf['spark.yarn.appMasterEnv.LD_LIBRARY_PATH'] = self._conf['spark.yarn.appMasterEnv.LD_LIBRARY_PATH'] + ':' + os.environ['LD_LIBRARY_PATH']
        else:
            self._conf['spark.yarn.appMasterEnv.LD_LIBRARY_PATH'] = os.environ['LD_LIBRARY_PATH']

        #adjust PYTHONPATH on executor
        if 'spark.yarn.executorEnv.PYTHONPATH' in self._conf:
           self._conf['spark.executorEnv.PYTHONPATH'] = self._conf['spark.executorEnv.PYTHONPATH'] + ':' + os.environ['PYTHONPATH']
        else:
           self._conf['spark.executorEnv.PYTHONPATH'] = os.environ['PYTHONPATH']

        # adjust LD_LIBRARY_PATH on executor
        if 'spark.executorEnv.LD_LIBRARY_PATH' in self._conf:
            self._conf['spark.executorEnv.LD_LIBRARY_PATH'] = self._conf['spark.executorEnv.LD_LIBRARY_PATH'] + ':' + os.environ['LD_LIBRARY_PATH']
        else:
            self._conf['spark.executorEnv.LD_LIBRARY_PATH'] = os.environ['LD_LIBRARY_PATH']

        nxcals_jars = subprocess.run(['ls $LCG_VIEW/nxcals/nxcals_java/* | xargs | sed -e "s/ /:/g"'], shell=True, stdout=subprocess.PIPE, env=os.environ).stdout.decode('utf-8')

        # add nxcals jars on driver
        if 'spark.driver.extraClassPath' in self._conf:
            self._conf['spark.driver.extraClassPath'] = self._conf['spark.driver.extraClassPath'] + ':' + nxcals_jars
        else:
            self._conf['spark.driver.extraClassPath'] = nxcals_jars

        # add nxcals jars on executor
        if 'spark.executor.extraClassPath' in self._conf:
            self._conf['spark.executor.extraClassPath'] = self._conf['spark.executor.extraClassPath'] + ':' + nxcals_jars
        else:
            self._conf['spark.executor.extraClassPath'] = nxcals_jars

        # add nxcals and hadoop keytabs
        self._keytab = hadoop_keytab
        self._principal = hadoop_principal

        # distribute nxcals keytab to be available on the driver
        if 'spark.yarn.dist.files' in self._conf:
            self._conf['spark.yarn.dist.files'] = self._conf['spark.yarn.dist.files'] + ',' + '%s#nxcals.keytab,' % nxcals_api_keytab
        else:
            self._conf['spark.yarn.dist.files'] = '%s#nxcals.keytab' % nxcals_api_keytab

        # add log4j config
        log4j_conf = os.path.join(os.path.dirname(os.path.realpath(__file__)),'log4j.properties')
        os.environ['SPARK_SUBMIT_OPTS'] = "-Dlog4j.configuration=%s" % log4j_conf
        self._conf['spark.yarn.dist.files'] = self._conf['spark.yarn.dist.files'] + ',' + '%s' % log4j_conf

        # add nxcals api service endpoints and keytab
        extra_java_opts = "-Dservice.url=\"https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093\" -Dkerberos.principal=%s -Dkerberos.keytab=./nxcals.keytab" % nxcals_api_principal
        if 'spark.driver.extraJavaOptions' in self._conf:
            self._conf['spark.driver.extraJavaOptions'] = self._conf['spark.driver.extraJavaOptions'] + ' ' + extra_java_opts
        else:
            self._conf['spark.driver.extraJavaOptions'] = extra_java_opts

        self.log.info('Defined configuration for spark submit job %s' % self._name)

    def execute(self, context):
        # prepare spark submit for nxcals
        self._prepare_spark_submit()

        # execute
        super(SparkNXCALSOperator, self).execute(context)
