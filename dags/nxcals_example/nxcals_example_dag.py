from __future__ import print_function

import os
import shutil
import subprocess
import tempfile

from airflow.utils.dates import days_ago
from airflow.models import DAG
from airflow.exceptions import AirflowException
from airflow.operators.python_operator import PythonOperator

from airflow.operators.nxcals_plugin import SparkNXCALSOperator

args = {
    'owner': 'airflow',
    'start_date': days_ago(2),
}

dag = DAG(
    dag_id='nxcals_example',
    default_args=args,
    schedule_interval=None,
    tags=['nxcals']
)

# [START zip_operator_run]
# create lhcsmapi.zip file containing pip packages to be submitted
lhcsm_version = '1.3.106'

def zip_lhcsmapi_package():
    if os.path.exists("/tmp/lhcsmapi-%s.zip" % lhcsm_version):
        print('Zip already packaged')
        return

    with tempfile.TemporaryDirectory() as tmpdir:
        cmd = 'rm -rf %s; ' % tmpdir +\
              'pip3 install -t %s tzlocal; ' % tmpdir +\
              'pip3 install -t %s influxdb; ' % tmpdir +\
              'pip3 install -t %s lhcsmapi==%s;' % (tmpdir, lhcsm_version)
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, close_fds=True, universal_newlines=True)
        for line in p.stdout:
            print(line.rstrip())
        if p.poll() != 0:
            raise AirflowException("packaging lhcsmapi failed")
        shutil.make_archive("/tmp/lhcsmapi-%s" % lhcsm_version, 'zip', tmpdir)

zip_operator_run = PythonOperator(task_id='zip_lhcsmapi', python_callable=zip_lhcsmapi_package, dag=dag)

# [END zip_operator_run]

# [START nxcals_operator_run]
# run nxcals jobs with lhcsmapi python package

dn = os.path.dirname(os.path.realpath(__file__))

nxcals_operator_run = SparkNXCALSOperator(
    application=os.path.join(dn, 'nxcals_example_job.py'),
	task_id='nxcals_query',
    dag=dag,
    archives="/tmp/lhcsmapi-%s.zip" % lhcsm_version,
    conf={ 'spark.yarn.appMasterEnv.PYTHONPATH' : "lhcsmapi-%s.zip" % lhcsm_version },
)

# [END nxcals_operator_run]

zip_operator_run >> nxcals_operator_run