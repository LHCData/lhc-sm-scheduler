from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession
from cern.nxcals.pyquery.builders import *
import matplotlib

from lhcsmapi.dbsignal.Signal import Signal

# init spark session
conf = SparkConf()
sc = SparkContext(conf=conf)
spark = SparkSession(sc)

# make sure graphs are skipped
matplotlib.use("Agg")

# query some arbitrary system using NXCALS DevicePropertyQuery Api
start_time = "2018-07-10 00:00:00.000"
end_time = "2018-07-31 23:00:00.000"
df = DevicePropertyQuery\
	.builder(spark)\
	.system("CMW")\
	.startTime(start_time)\
	.endTime(end_time)\
	.entity()\
	.deviceLike("MKD.UA63.SCSS.AB1")\
	.propertyLike("XpocData")\
	.buildDataset()\
	.select('acqStamp')\
	.toPandas()

print("FOUND NXCALS DevicePropertyQuery %d" % len(df))


# query using LHCSM API for supported system
bmode_df = Signal().read('nxcals', nxcals_system='CMW',
              signal='HX:BMODE', t_start='2015-03-13 05:20:59.4910002', t_end='2015-04-13 05:20:59.4910002', spark=spark)

print("FOUND LHCSM SignalQuery %d" % len(bmode_df))

sc.stop()
