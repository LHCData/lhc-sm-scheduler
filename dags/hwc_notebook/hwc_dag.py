import subprocess

import airflow
from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator

# Based on https://medium.com/@ntruong/airflow-externally-trigger-a-dag-when-a-condition-match-26cae67ecb1a

dag = DAG(
    dag_id='rest_trigger_hwc_dag',
    default_args={'start_date': airflow.utils.dates.days_ago(2), 'owner': 'AccTesting'},
    schedule_interval=None,
)

def trigger_notebook_execution(*args, **kwargs):
    print("Received a REST message: {}".
          format(kwargs['dag_run'].conf))

    circuit_type = kwargs['dag_run'].conf['circuit_name'].split('.')[0]
    test_type = kwargs['dag_run'].conf['test_type']
    circuit_name_var = 'variables[CIRCUIT_NAME]=' + kwargs['dag_run'].conf['circuit_name']
    notebook_name_var = 'variables[NOTEBOOK_NAME]={}/AN_{}_{}'.format(circuit_type.lower(), circuit_type, test_type)
    start_time_var = 'variables[START_TIME]=' + kwargs['dag_run'].conf['start_time']
    end_time_var = 'variables[END_TIME]=' + kwargs['dag_run'].conf['end_time']

    command = ['curl', '-X', 'POST', '-F', 'token=0a53a152068d7fd59cb67c7b010807',
           '-F', 'variables[CLUSTER]=hadoop-nxcals',
           '-F', circuit_name_var,
           '-F', notebook_name_var,
           '-F', start_time_var, 
           '-F', end_time_var,
           '-F', 'ref=master',
           'https://gitlab.cern.ch/api/v4/projects/90588/trigger/pipeline']
    print(' '.join(command))
    return subprocess.run(command, stdout=subprocess.PIPE).stdout.decode('utf-8')    

run_this = PythonOperator(
    task_id='trigger_notebook_execution',
    python_callable=trigger_notebook_execution,
    provide_context=True,
    dag=dag,
)
