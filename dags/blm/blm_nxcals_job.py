import pandas as pd
from time import sleep
import matplotlib
from datetime import datetime

# NXCALS and Spark libraries
from cern.nxcals.pyquery.builders import *
from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession

# Internal libraries
from lhcsmapi.Time import Time
from lhcsmapi.pyedsl.QueryBuilder import QueryBuilder
from lhcsmapi.analysis.MonitoringUtils import get_checkpoint_timestamp, save_row_with_checkpoint
from lhcsmapi.analysis.BlmAnalysis import get_lhc_context, get_unique_blm_timestamps, calculate_blm_feature_row

# init spark session
conf = SparkConf()
sc = SparkContext(conf=conf)
spark = SparkSession(sc)

# make sure graphs are skipped
matplotlib.use("Agg")

# get configuration
blm_output_path = conf.get(key='spark.lhcsm.blm.output', defaultValue='/project/lhcsm/blm/partition=2015-11-customrange')
blm_start_time = conf.get(key='spark.lhcsm.blm.start', defaultValue='2015-11-23 07:28:53+01:00') # '2018-05-01 00:00:00'
blm_end_time = conf.get(key='spark.lhcsm.blm.end', defaultValue='2018-05-15 00:00:00')
blm_search_interval_ns = 6*3600000000000  # 6 hours

features = ['max']

# 2.1. Compute BLM features with checkpointing
# get a checkpoint from which to start/resume and where to end compute.
blm_start_epoch_ns = get_checkpoint_timestamp(path=blm_output_path, spark=spark, sc=sc)
blm_end_epoch_ns = Time.to_unix_timestamp(blm_end_time)

if not blm_start_epoch_ns:
    # if checkpoint is not existing, then start from blm_start_time
    blm_start_epoch_ns = Time.to_unix_timestamp(blm_start_time)

# compute blm
print("Start computing complete BLMs from %s to %s\n" % (
    Time.to_string_short(blm_start_epoch_ns),
    Time.to_string_short(blm_end_epoch_ns)))

finished = False
t_search_start = blm_start_epoch_ns
t_search_end = blm_start_epoch_ns

features = ['max']

stop = 1

while (not finished):
    if Time.to_unix_timestamp(datetime.now()) < t_search_start:
        # the search is in the future, wait 5 minutes before executing query for data to populate
        print("Waiting 5 minutes for new beam mode data..")
        sleep(5 * 60)

    # search until t_search_end + search_interval_ns or t_end
    t_search_end = min(t_search_end + blm_search_interval_ns, blm_end_epoch_ns)

    # get busbars for selected range
    print("Start searching BLM PM events from %s to %s" % (
    Time.to_string_short(t_search_start), Time.to_string_short(t_search_end)))

    source_timestamp_df = QueryBuilder().with_pm() \
        .with_duration(t_start=t_search_start, t_end=t_search_end) \
        .with_query_parameters(system='BLM', className='BLMLHC', source='*') \
        .event_query().df
    if source_timestamp_df.empty == True:
        continue

    timestamps = get_unique_blm_timestamps(source_timestamp_df)

    if (timestamps.empty == True) and (t_search_end >= blm_end_epoch_ns):
        print("Finished, compute interval %s to %s has been exceeded: %s" % (
            Time.to_string_short(blm_start_epoch_ns),
            Time.to_string_short(blm_end_epoch_ns),
            Time.to_string_short(t_search_end)))
        finished = True
        continue

    for index, row in timestamps.iterrows():
        timestamp = row['timestamp']

        print("Process BLM for timestamp(%s)" % Time.to_string_short(timestamp))
        lhc_context_row_df = get_lhc_context(timestamp)

        mask = (source_timestamp_df['timestamp'] > timestamp - 100e9) & (
                    source_timestamp_df['timestamp'] < timestamp + 100e9)
        subsource_timestamp_df = source_timestamp_df[mask]
        blm_feature_row_df = calculate_blm_feature_row(subsource_timestamp_df, features, lhc_context_row_df.index,
                                                       signal='pmTurnLoss')
        out_row_df = pd.concat([lhc_context_row_df, blm_feature_row_df], axis=1)

        # start next search from the end of processed signal
        t_search_start = int(timestamp + 100e9)

        print("Checkpoint at the last processed timestamp t_search_start %s" % (Time.to_string_short(t_search_start)))
        save_row_with_checkpoint(pandas_row=out_row_df, path=blm_output_path, t_checkpoint=t_search_start, spark=spark,
                                 sc=sc)

    # For debugging
    if stop == 2:
        break

    stop += 1

print("Finished, compute interval %s to %s has been exceeded: %s" % (
    Time.to_string_short(blm_start_epoch_ns),
    Time.to_string_short(blm_end_epoch_ns),
    Time.to_string_short(t_search_end)))

sc.stop()
