from __future__ import print_function

import sys
import os
import shutil
import subprocess
import tempfile

from airflow.utils.dates import days_ago
from airflow.models import DAG, Variable
from airflow.exceptions import AirflowException
from airflow.operators.python_operator import PythonOperator

from airflow.operators.nxcals_plugin import SparkNXCALSOperator

args = {
    'owner': 'airflow',
    'start_date': days_ago(2),
}

# Get Airflow Variable
busbar_config = Variable.get("busbar", deserialize_json=True, default_var={"t_start": "2017-01-01 00:00:00", "t_end": "2017-01-31 00:00:00", "output_path": "/project/lhc_signal_monitoring/busbar/rb/partition=2017", "circuit_type": "RB", "signal_type": "U_RES", "lhcsm_version": "1.3.143", "force_rebuild": False})

lhcsm_version = busbar_config['lhcsm_version']

# create lhcsmapi.zip file containing pip packages to be submitted
def zip_lhcsmapi_package():
    if not busbar_config['force_rebuild'] and os.path.exists("/tmp/lhcsmapi-%s.zip" % lhcsm_version):
        print('Zip already packaged')
        return

    with tempfile.TemporaryDirectory() as tmpdir:
        cmd = 'rm -rf %s; ' % tmpdir +\
              'pip3 install -t %s tzlocal; ' % tmpdir +\
              'pip3 install -t %s influxdb; ' % tmpdir + \
              'pip3 install -t %s tqdm; ' % tmpdir + \
              'pip3 install -t %s lhcsmapi==%s;' % (tmpdir, lhcsm_version)
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, close_fds=True, universal_newlines=True)
        for line in p.stdout:
            print(line.rstrip())
        if p.poll() != 0:
            raise AirflowException("packaging lhcsmapi failed")
        shutil.make_archive("/tmp/lhcsmapi-%s" % lhcsm_version, 'zip', tmpdir)


with DAG(dag_id='busbar_nxcals_dag', default_args=args, schedule_interval=None, tags=['nxcals']) as dag:
    # [START zip_operator_run]
    zip_operator_run = PythonOperator(task_id='zip_lhcsmapi', python_callable=zip_lhcsmapi_package)
    # [END zip_operator_run]

    # [START nxcals_operator_run]
    # run nxcals jobs with lhcsmapi python package
    dn = os.path.dirname(os.path.realpath(__file__))

    nxcals_operator_run = SparkNXCALSOperator(
        application=os.path.join(dn, 'busbar_nxcals_job.py'),
        task_id='busbar_nxcals_query',
        archives="/tmp/lhcsmapi-%s.zip" % lhcsm_version,

        conf={'spark.yarn.appMasterEnv.PYTHONPATH': "lhcsmapi-%s.zip" % lhcsm_version,
              'spark.lhcsm.busbar.start': busbar_config['t_start'],
              'spark.lhcsm.busbar.end': busbar_config['t_end'],
              'spark.lhcsm.busbar.output': busbar_config['output_path'],
              'spark.lhcsm.busbar.circuit_type': busbar_config['circuit_type'],
              'spark.lhcsm.busbar.signal_type': busbar_config['signal_type'],
              'spark.executor.instances': 10
              },
    )
    # [END nxcals_operator_run]

    zip_operator_run >> nxcals_operator_run
