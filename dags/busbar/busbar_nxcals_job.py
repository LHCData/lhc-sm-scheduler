import pandas as pd
from time import sleep
import matplotlib
from datetime import datetime

# NXCALS and Spark libraries
from cern.nxcals.pyquery.builders import *
from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession

# Internal libraries
from lhcsmapi.Time import Time
from lhcsmapi.analysis.MonitoringUtils import get_checkpoint_timestamp, save_row_with_checkpoint
from lhcsmapi.analysis.busbar.BusbarResistanceAnalysis import get_complete_busbar_df, get_busbar_resistances

# init spark session
conf = SparkConf()
sc = SparkContext(conf=conf)
spark = SparkSession(sc)

# make sure graphs are skipped
matplotlib.use("Agg")

# get configuration
busbar_output_path = sc.getConf().get(key='spark.lhcsm.busbar.output', defaultValue='/user/mmacieje/busbar/partition=2018-04-customrange')
busbar_start_time = sc.getConf().get(key='spark.lhcsm.busbar.start', defaultValue='2018-04-01 00:00:00')
busbar_end_time = sc.getConf().get(key='spark.lhcsm.busbar.end', defaultValue='2018-04-05 00:00:00')
busbar_search_interval_ns = 6*3600000000000  # 6 hours

circuit_type = sc.getConf().get(key='spark.lhcsm.busbar.circuit_type', defaultValue='RB')
signal_type = sc.getConf().get(key='spark.lhcsm.busbar.signal_type', defaultValue='U_RES')

# LHCSMAPI version
import lhcsmapi
print('Analysis executed with lhcsmapi version: {}'.format(lhcsmapi.__version__))

lhcsmapi_major, lhcsmapi_minor, lhcsmapi_patch = map(int, lhcsmapi.__version__.split('.'))
lhcsmapi_version_dct = {'lhcsmapi_major': lhcsmapi_major,
                        'lhcsmapi_minor': lhcsmapi_minor,
                        'lhcsmapi_patch': lhcsmapi_patch}

# 2.1. Compute busbar with checkpointing
# get a checkpoint from which to start/resume and where to end compute.
busbar_start_epoch_ns = get_checkpoint_timestamp(path=busbar_output_path, spark=spark, sc=sc)
busbar_end_epoch_ns = Time.to_unix_timestamp(busbar_end_time)

if not busbar_start_epoch_ns:
    # if checkpoint is not existing, then start from busbar_start_time
    busbar_start_epoch_ns = Time.to_unix_timestamp(busbar_start_time)

# compute busbar
print("Start computing complete busbars from %s to %s" % (
    Time.to_string_short(busbar_start_epoch_ns),
    Time.to_string_short(busbar_end_epoch_ns)))

finished = False
t_search_start = busbar_start_epoch_ns
t_search_end = busbar_start_epoch_ns
bmode_pattern = {'SETUP': 2.0, 'PRERAMP': 6.0, 'FLATTOP': 8.0, 'RAMPDOWN': 14.0}

while (not finished):
    if Time.to_unix_timestamp(datetime.now()) < t_search_start:
        # the search is in the future, wait 5 minutes before executing query for data to populate
        print("Waiting 5 minutes for new beam mode data..")
        sleep(5 * 60)

    # search until t_search_end + search_interval_ns or t_end
    t_search_end = min(t_search_end + busbar_search_interval_ns, busbar_end_epoch_ns)

    # get busbars for selected range
    busbar_df = get_complete_busbar_df(t_start=t_search_start, t_end=t_search_end, pattern=bmode_pattern, spark=spark)

    if (busbar_df.empty is True) and (t_search_end >= busbar_end_epoch_ns):
        finished = True
        continue

    for index, row in busbar_df.iterrows():
        t_start_inj = row['t_start_injs']
        t_end_inj = row['t_end_injs']
        t_start_sb = row['t_start_sbs']
        t_end_sb = row['t_end_sbs']

        print("Process busbar for t_start_inj(%s) t_end_inj(%s) t_start_sb(%s) t_end_sb(%s)" % (
            Time.to_string_short(t_start_inj),
            Time.to_string_short(t_end_inj),
            Time.to_string_short(t_start_sb),
            Time.to_string_short(t_end_sb)))

        res_row_df = get_busbar_resistances(circuit_type, signal_type, t_start_inj, t_end_sb, spark=spark)
        if res_row_df.empty:
            print('Current profiles across circuits are different. Computation of this particular ramp is terminated.')
            t_search_start = int(t_end_sb)
            continue

        time_row_df = pd.DataFrame(row.to_dict(), index=[t_start_inj])
        lhcsmapi_row_df = pd.DataFrame(lhcsmapi_version_dct, index=[t_start_inj])
        out_row_df = pd.concat([time_row_df, lhcsmapi_row_df, res_row_df], axis=1)

        # start next search from the end of processed signal
        t_search_start = int(t_end_sb)

        print("Checkpoint at the last processed timestamp t_search_start %s" % (Time.to_string_short(t_search_start)))
        save_row_with_checkpoint(pandas_row=out_row_df, path=busbar_output_path, t_checkpoint=t_search_start, spark=spark, sc=sc)

print("Finished, compute interval %s to %s has been exceeded: %s" % (
    Time.to_string_short(busbar_start_epoch_ns),
    Time.to_string_short(busbar_end_epoch_ns),
    Time.to_string_short(t_search_end)))

sc.stop()
