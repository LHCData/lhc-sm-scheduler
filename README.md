# LHC Signal Monitoring Scheduler

This repository is storing production Airflow DAGs for Spark/NXCALS and Openshift deployment files, which are based on example done in https://gitlab.cern.ch/db/spark-service/airflow

## Dags

Dags pushed to this repository under `dags` folder will be deployed with git-sync automatically to production [https://lhc-sm-scheduler.web.cern.ch](https://lhc-sm-scheduler.web.cern.ch) AirFlow instance running on CERN Openshift

## NXCALS/Spark plugin

[NXCALS Plugin for Airflow] is a wrapper over Spark AirFlow Plugin that provides seemless access to NXCALS from DAGs.

## Building Docker Image

1. Install Docker from https://docs.docker.com/docker-for-windows/install/
2. Clone this repository locally with
```bash
git clone https://gitlab.cern.ch/lhcdata/lhc-sm-scheduler
```
3. Enter the repository:
```bash
cd lhc-sm-scheduler
```

4. Build the docker image
**Please check the latest image at https://gitlab.cern.ch/LHCData/lhc-sm-scheduler/container_registry and increment the version by one prior to executing the command below**

```bash
docker build -t gitlab-registry.cern.ch/lhcdata/lhc-sm-scheduler/airflow:v1.10.9-21 ./scheduler
```

5. Push the Docker image to the repository
- Login to GitLab registry
```bash
docker login gitlab-registry.cern.ch
```
Provide the NICE username and password as requested.
- Push the created docker image
```bash
docker push gitlab-registry.cern.ch/lhcdata/lhc-sm-scheduler/airflow:v1.10.9-21
```

**Note that after creating a new docker image, the version has to be updated in openshift.yml**

## Deployments

### Local testing deployment

Start docker

```bash
docker-compose up
```
This command will launch the Airflow scheduler and webserver with the dags available in the repository. This is very useful for the local testing of DAGs prior to sending for production.

Access local Airflow instance at http://localhost:8080 in any browser.

### Openshift deployment

1. generate NXCALS API and HADOOP keytabs
  - Login to LXPLUS
  ```bash
  ssh mmacieje@lxplus.cern.ch
  ```
  - Generate keytabs by executing (e.g., `lhcsm` as `<service-account>`)
    ```bash
    cern-get-keytab --login <service-account> --user --password <service-account-pass> --keytab nxcals.keytab
    cern-get-keytab --login <service-account> --user --password <service-account-pass> --keytab hadoop.keytab
    ```
  - Copy keytabs to a local directory, e.g., to EOS folder
    ```bash
    mv nxcals.keytab /eos/user/m/mmacieje
    mv hadoop.keytab /eos/user/m/mmacieje
    ```

2. Login to your project at https://openshift.cern.ch and open your project; the project is also directly accessible through https://openshift.cern.ch/console/project/lhc-sm-scheduler/overview
- Create secret `spark-keytabs` in Openshift UI (as referenced in `openshift.yml`).   
  Secret should have `hadoop.keytab` and `nxcals.keytab` paths that will be mounted to Airflow Scheduler
  - Open from the left panel Resources -> Secrets 
  - Click button `Create Secret` in the right-top corner
  - Fill in the fields as shown below

  ![setting-keytab](figures/setting-keytabs.png)
  
  **When the lhcsm acount password expires, the keytabs have to be regenerated and accordingly uploaded to OpenShift secrets.**

3. Go to Overview in the left panel and select import YAML/JSON.
  - Copy the content of file `openshift.yml` into the opened window and click `Create`  
  This file provides a configuration for the Openshift.

  ![import-yml-create](figures/import-yml-create.png)
  - In the next window keep the default settings as shown below and click `Continue`

  ![import-yml-create-default](figures/import-yml-create-default.png)
  - Open shift will parse the `openshift.yml` and display the template configuration

  ![import-yml-create-template-configuration](figures/import-yml-create-template-configuration.png)
  - Click `Create` to confirm creation of the application following the template configuration

  ![import-yml-create-created](figures/import-yml-create-created.png)
  - Click `Close` to terminate the import of the template configuration
  - After deploying the service, note the networking field 

  ![service-deployed-networking](figures/service-deployed-networking.png)

4. Add SSO to the project in order to authenticate loging to the Airflow instance with CERN SSO (Single Sign-On)
  - Copy name 'lhc-sm-scheduler' from the netwirking field as shown in the previous image
  - Click `Add to Project` in the top-right corner

  ![add-to-project](figures/add-to-project.png)
  - Select `Browse Catalog`
  - Click `cern-sso-proxy` as shown below

  ![add-cern-sso-proxy](figures/add-cern-sso-proxy.png)
  - Click `Next`

  ![add-cern-sso-proxy-next](figures/add-cern-sso-proxy-next.png)
  - Fill the fields as shown below  
    AUTHORIZED_GROUPS: lhc-sm-users
    SERVICE_NAME: lhc-sm-scheduler (as copied from the networking field)

  ![add-cern-sso-proxy-configuration](figures/add-cern-sso-proxy-configuration.png)
  - Click `Create`

  ![add-cern-sso-proxy-created](figures/add-cern-sso-proxy-created.png)
  - Click `Close`
  
5. Enable /api endpoint for triggering DAGs through REST API
  - Go to `Resources` in the left panel
  - Select `Config Maps`

  ![config-maps](figures/config-maps.png)
  - Select `cern-sso-proxy`

  ![config-maps-cern-sso-proxy](figures/config-maps-cern-sso-proxy.png)
  - Click `Actions` and select `Edit` 
    
    From 
    ```bash#Protected resources, if you need to protect specific urls please change the Location path
    <Location "/">
    ```
    ![config-maps-cern-sso-proxy-edit-before](figures/config-maps-cern-sso-proxy-edit-before.png)
    To 
    ```bash#Protected resources, if you need to protect specific urls please change the Location path
    <Location ~ "^((?!/api).)*$">
    ```
    ![config-maps-cern-sso-proxy-edit-before](figures/config-maps-cern-sso-proxy-edit-after.png)
  - The change is not yet deployed. To this end, please select from the left panel `Applications`, then `Deploments`, `cern-sso-proxy`

    ![applications-deployments-cern-sso-proxy](figures/applications-deployments-cern-sso-proxy.png)
  - Click `Deploy` to deploy the updated SSO settings

    ![applications-deployments-cern-sso-proxy-deploy](figures/applications-deployments-cern-sso-proxy-deploy.png)
  - Now, accessing the website `https://lhc-sm-scheduler.web.cern.ch/apo` from CERN network yields the following preview

  ![updated-sso-settings](figures/updated-sso-settings.png)

# Removing the deployment on OpenShift
Recreating Airflow
1. Click `Applications`
  - Click `Deployments`
    - Select `dc-lhc-sm-scheduler`
      - Click `Actions`
      - Select `Delete`
2. Click `Applications`
  - Click `Deployments`
    - Select `cern-sso-proxy`
      - Click `Actions`
      - Select `Delete`
3. Click `Applications`
  - Click `Services`
    - Select `cern-sso-proxy`
      - Click `Actions`
      - Select `Delete`
4. Click `Applications`
  - Click `Services`
    - Select `lhc-sm-scheduler`
      - Click `Actions`
      - Select `Delete`
5. Click `Applications`
  - Click `Provisioned Services`
    - Select `cern-sso-proxy`
      - Click `Actions`
      - Select `Delete`
6. Click `Applications`
  - Click `Routes`
    - Select `cern-sso-proxy`
      - Click `Actions`
      - Select `Delete`
7. Click `Resources`
  - Click `Secrets`
    - Select `spark-keytabs`
      - Click `Actions`
      - Select `Delete`
8. Click `Resources`
  - Click `Other Resources`
    - Select `Service Account`
      - For `sa-lhcsm`
      - Click `Actions`
      - Select `Delete`
9. Click `Storage`
  - Click `cephfs`
    - Click `Actions`
    - Select `Delete`
10. Click `Storage`
  - Click `cvmfs-sft-cern-ch`
    - Click `Actions`
    - Select `Delete`

